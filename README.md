# Overview
Kubernetes OAuth2-proxy deployment to add auth to any app. 
OAuth2 Proxy is a reverse proxy that sits in front of your application and handles the complexities of OpenID Connect / OAuth 2.0 for you; requests that make it to your application have already been authorized!

Two different deployments are proposed here: 

1- **standalone** (preferred): ```k8s/standalone```

A single replica oauth2-proxy deployment, service, and ingress rule pointing to a webapp service deployment (i.e., with multiple replicas).

**Note:** Decoupled implementation, slightly higher code footprint. 

2- **sidecar**: ```k8s/sidecar``` 

Multiple instances of oauth2-proxy containers inside each webapp deployment pod. 
Each pod contains two containers:  webapp and an instance of the oauth2-proxy. The webapp-svc (i.e, service) in front of the webapp  balances the load and points to oauth2-proxy containers inside the pod. Ingress points to webapp-svc. 
   
**Note:** Container duplication per app. smaller code footprint. Direct pod communication with app. 

Both deployments default parameters have been tested with Azure Active Directory. By default allows multi-tenant support and in most browsers (i.e., Chrome, Safari, and Firefox). For a full list of configuration
parameters regarding to [oauth2-proxy](https://oauth2-proxy.github.io/oauth2-proxy/docs/). 

- [Command Line Options](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview#command-line-options) 

- [Environment Variables](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview#environment-variables)

- [Config File](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview#config-file)

- [Endpoints](https://oauth2-proxy.github.io/oauth2-proxy/docs/features/endpoints)

# Quick Start

## Registering your app in Azure Active Directory 

Tutorial to register your app and restrict its access to a set of users or groups [here](https://docs.microsoft.com/en-us/azure/active-directory/develop/howto-restrict-your-app-to-a-set-of-users)

1. Register you App in Azure Active Directory.
2. Set your Redirect URI Authentication  
   Azure Active Directory >> App Registrations >> Your App >> Authentication >> Redirect URIs. 
   - e.g.: **```https://<fqdn>/oauth2/callback```** (See Endpoints for reference)
   - e.g.: **```http://localhost:<port>/oauth2/callback```**
3. Gather OIDC client information values such as:
    - ```client-id``` : 

        Azure Active Directory >> App Registration >> Your App >> Application ID
    - ```client-secret```: 

       Azure Active Directory >> App Registration >> Your App >> Certificates & Secrets >> New Client Secret 
    - ```redirect-url```: 
    
        same url as Step 2 (e.g., ```https://<fqdn>/oauth/callback```)
    - ```tenant-id```: 

        Azure Active Directory >> App Registration >> Your App >> Directory tenant ID
4. Generate Cookie Secret: 
    - ```cookie-secret```: 
        
    ```python -c 'import os,base64; print(base64.b64encode(os.urandom(16)))'```


## Deployment

1. Clone this repository.

2. Set OIDC client information in k8s file ```oauth2-proxy-secrets.yaml```

Note: all data is being entered in basic text form, not base64. For more "security" use the field ```data``` instead of ```stringData```.
```yaml
apiVersion: v1
kind: Secret 
metadata:
  name: oauth2-proxy-secrets
stringData:
  OAUTH2_PROXY_CLIENT_ID: <client-id>
  OAUTH2_PROXY_CLIENT_SECRET: <client-secret>
  OAUTH2_PROXY_REDIRECT_URL: <redirect-url>
  OAUTH2_PROXY_OIDC_ISSUER_URL: https://login.microsoftonline.com/<tenant-id>/v2.0
  OAUTH2_PROXY_COOKIE_SECRET: <cookie-secret>
```
3. Set the correct FQDN for the k8s Ingress rule

- **standalone:** ```k8s/standalone/oauth2-ingress.yaml``` line 9

- **sidecar:** ```k8s/sidecar/oauth2-ingress.yaml``` line 9

Note 1: in case that you don't have a real FQDN, edit ```/etc/hosts``` an add line:
```
127.0.0.1 <fqdn>
```

Note 2: If working with ```localhost:<port>``` instead of ```<fqdn>```, skip the Ingress deployment and forward the correspondent service port (i.e, ```oauth2-proxy-svc``` for standalone, ```webapp-svc``` for sidecar). 


4. Apply the deployment. 

```kubectl apply -f k8s/standalone/``` 
```kubectl apply -f k8s/sidecar/```
